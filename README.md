## Getting Started

Install node packages:

```bash
npm i
```

Run the development server and node server:

```bash
npm run dev

node server.js
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Authorization info
login: **login**
password: **password**
