const path = require('path')
const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router(path.join(__dirname, 'db.json'))
console.log('Data base path: ', path.join(__dirname, 'db.json'))
const middlewares = jsonServer.defaults()

server.use(middlewares)

server.use(jsonServer.bodyParser)
server.post('/contacts', (req, res, next) => {
  next()
})
server.post('/auth/token', (req, res, next) => {
  if (req.body.login === 'login' && req.body.password === 'password') {
    res.status(200).send({
      userToken: '89ohjfn329u1jkb431k4nkfvud89s',
    })
  } else {
    res.status(400).send({
      error: 'Incorrect login or password',
    })
  }
})

server.use(router)
server.listen(6001, () => {
  console.log('JSON Server is listening on port 6001')
})
