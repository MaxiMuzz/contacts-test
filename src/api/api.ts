import axios from 'axios'

export interface IContact {
  id: string;
  key: string;
  name: string;
  phone: string;
  lastName?: string;
  address?: string;
  comment?: string;
}

const BASE_PATH = 'http://localhost:6001'

export const getContacts = async () => {
  try {
    const { data }: { data: IContact[] } = await axios.get(`${BASE_PATH}/contacts`)

    return { data, error: null }
  } catch (error) {
    return { data: null, error }
  }
}

export const createContact = async (body: IContact) => {
  try {
    const { data }: { data: IContact } = await axios.post(`${BASE_PATH}/contacts`, body)

    return { data, error: null }
  } catch (error) {
    return { data: null, error }
  }
}

export const updateContact = async (id: string, body: IContact) => {
  try {
    const { data }: { data: IContact } = await axios.patch(`${BASE_PATH}/contacts/${id}`, body)

    return { data, error: null }
  } catch (error) {
    return { data: null, error }
  }
}

export const deleteContact = async (id: string) => {
  try {
    await axios.delete(`${BASE_PATH}/contacts/${id}`)

    return { data: { id }, error: null }
  } catch (error) {
    return { data: null, error }
  }
}

export interface UserData {
  login: string;
  password: string;
}

export const authorize = async (userData: UserData) => {
  try {
    const { data }: { data: { userToken: string } } = await axios.post(`${BASE_PATH}/auth/token`, userData)

    return { data, error: null }
  } catch (error) {
    return { data: null, error }
  }
}
