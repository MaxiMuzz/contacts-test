import { IContact } from '../api/api'

interface IContactsState {
  contacts: IContact[];
}

export enum ContactsActionTypes {
  SET,
  ADD,
  DELETE,
  UPDATE,
}

interface SetAction {
  type: ContactsActionTypes.SET;
  payload: { data: IContact[]};
}

interface AddAction {
  type: ContactsActionTypes.ADD;
  payload: { data: IContact }
}

interface DeleteAction {
  type: ContactsActionTypes.DELETE
  payload: { id: string; }
}

interface UpdateAction {
  type: ContactsActionTypes.UPDATE;
  payload: {
    id: string;
    data: IContact;
  }
}

export type ContactsAction = SetAction | AddAction | DeleteAction | UpdateAction

const initialState: IContact[] = []

const contactsReducer = (state = initialState, action: ContactsAction): IContact[] => {
  switch(action.type) {
    case ContactsActionTypes.SET:
      return action.payload.data

    case ContactsActionTypes.ADD:
      return [...state, action.payload.data]

    case ContactsActionTypes.DELETE:
      return state.filter((contact) => contact.id !== action.payload.id)

    case ContactsActionTypes.UPDATE:
      return state.map((contact) => 
        contact.id === action.payload.id
          ? action.payload.data
          : contact
        )

    default: 
      return state
  }
}

export default contactsReducer
