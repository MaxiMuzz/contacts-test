import { combineReducers } from 'redux'
import { IContact } from '../api/api'
import contactsReducer from './contactsReducer'

export interface IStore {
  contacts: IContact[]
}

const rootReducer = combineReducers({
  contacts: contactsReducer,
})

export default rootReducer
