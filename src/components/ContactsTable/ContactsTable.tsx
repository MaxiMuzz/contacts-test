import { Button, Col, Input, Modal, notification, Row, Table } from 'antd'
import { useState } from 'react'
import styles from './ContactsTable.module.scss'
import { DeleteFilled, EditFilled, SearchOutlined } from '@ant-design/icons'
import { ContactModal } from '../ContactModal'
import { useRouter } from 'next/router'
import { IContact } from '../../api/api'
import api from '../../api'
import { ContactsAction, ContactsActionTypes } from '../../store/contactsReducer'
import { useDispatch } from 'react-redux'
import { Dispatch } from 'redux'

const { Column } = Table
const { confirm } = Modal

interface IProps {
  contacts: IContact[];
}

export const ContactsTable: React.FC<IProps> = ({ contacts }) => {
  const router = useRouter()
  const dispatch = useDispatch<Dispatch<ContactsAction>>()
  const locations = router.asPath.split('/')
  const edit = locations.includes('edit')
  const create = locations.includes('create')
  const editingContactId = locations[locations.length - 1]
  const [searchValue, setSearchValue] = useState('')
  const filteredContacts =
    searchValue
      ? contacts.filter(
        ({name, lastName, phone}) => {
          const fio = lastName ? `${name} ${lastName}` : name
          
          return (
            fio.toUpperCase().match(new RegExp(searchValue.toUpperCase())) ||
            phone.match(new RegExp(searchValue))
          )
        })
      : contacts

  const openModal = (id: string) => {
    if (id) router.push(`/edit/${id}`)
  }
  const closeModal = () => {
    router.push('/')
  }

  const deleteContact = (id: string) => {
    const deletedContact = filteredContacts.find(({ id: contactId }) => contactId === id)
    const contactFullName = deletedContact?.lastName 
      ? `${deletedContact?.name} ${deletedContact?.lastName}`
      : deletedContact?.name 

    confirm({
      title: `Вы уверены, что хотите удалить контакт "${contactFullName}"?`,
      okText: 'Да',
      cancelText: 'Нет',
      onOk() {
        api.deleteContact(id).then(({ data, error }) => {
          if (data) {
            dispatch({
              type: ContactsActionTypes.DELETE,
              payload: { id: id },
            })
    
            notification.success({
              message: 'Успешно',
              description: 'Контакт успешно удален',
            })
          }
    
          error && notification.error({ 
            message: 'Ошибка',
            description: 'В процессе удаления произошла непредвиденная ошибка',
          })
        })
      }
    })
  }

  return (
    <>
      <Row className={styles.searchRow}>
        <Col flex='1 1 auto'>
          <Input
            placeholder='Введите имя или телефон'
            onChange={(e) => 
              setSearchValue(e.target.value)
            }
            suffix={<SearchOutlined />}
          />
        </Col>
        <Col>
          <Button
            className={styles.addContact}
            type='primary'
            htmlType='button' 
            onClick={() => {
              router.push('/create')
            }}
          >
            Добавить контакт
          </Button>
          <Button
            className={styles.addContactMobile}
            type='primary'
            htmlType='button' 
            onClick={() => {
              router.push('/create')
            }}
          >
            +
          </Button>
        </Col>
      </Row>
      <Table
        dataSource={filteredContacts}
        onRow={(record) => ({
          onClick: () => window.innerWidth <= 500 && openModal(record.id)
        })}
      >
        <Column
          title='ФИО'
          key='fio'
          render={({ name, lastName }: IContact) => (
            lastName ? `${name} ${lastName}` : name
          )}
        />
        <Column
          title='Телефон'
          key='phone'
          dataIndex='phone'
        />
        <Column
          className={styles.additionalInfo}
          title='Адрес'
          key='address'
          dataIndex='address'
        />
        <Column
          className={styles.additionalInfo}
          title='Комментарий'
          key='comment'
          dataIndex='comment'
        />
        <Column
          className={styles.actionsColumn}
          title=''
          key='actions'
          render={({ id }: IContact) => (
            <Row wrap={false} justify='end'>
              <button 
                className={styles.actionButton}
                onClick={() => openModal(id)}
              >
                <EditFilled />
              </button>
              <button
                className={styles.actionButton}
                onClick={() => deleteContact(id)}
              >
                <DeleteFilled />
              </button>
            </Row>
          )}
        />
      </Table>
      <ContactModal
        visible={Boolean(edit) || Boolean(create)}
        mode={edit && editingContactId ? 'edit': 'create'}
        contactId={editingContactId}
        closeModal={closeModal}
        />
    </>
  )
}
