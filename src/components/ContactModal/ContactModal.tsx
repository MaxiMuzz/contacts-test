import { Form, Input, Modal, notification, Typography } from 'antd'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Dispatch } from 'redux'
import api from '../../api'
import { IContact } from '../../api/api'
import { REQUIRED_MESSAGE } from '../../constants'
import { ContactsAction, ContactsActionTypes } from '../../store/contactsReducer'
import { IStore } from '../../store/rootReducer'

interface IProps {
  visible: boolean;
  mode: 'edit' | 'create';
  closeModal: () => void;
  onOk?: () => void;
  contactId?: string;
}

export const ContactModal: React.FC<IProps> = ({ contactId, visible, mode, closeModal }) => {
  const contact = useSelector((state: IStore) => 
    state.contacts.find((contact) => contact.id === contactId)
  )
  const dispatch = useDispatch<Dispatch<ContactsAction>>()
  const [form] = Form.useForm()
  const onFinish = (values: IContact) => {
    if (mode === 'edit' && contactId) {
      api.updateContact(contactId, values).then(({ data, error }) => {
        if (data) {
          dispatch({
            type: ContactsActionTypes.UPDATE,
            payload: { id: contactId, data: data },
          })

          notification.success({
            message: 'Успешно',
            description: 'Контакт успешно сохранен',
          })

          closeModal()
        }
        
        error && notification.error({ 
          message: 'Ошибка',
          description: 'В процессе сохранения произошла непредвиденная ошибка',
        })
      })
    }

    if (mode === 'create') {
      const id = new Date().getTime().toString()
      
      api.createContact({ ...values, id, key: id })
        .then(({ data, error }) => {
          if (data) {
            dispatch({
              type: ContactsActionTypes.ADD,
              payload: { data: data },
            })
  
            notification.success({
              message: 'Успешно',
              description: 'Контакт успешно добавлен',
            })
  
            closeModal()
          }
          
          error && notification.error({ 
            message: 'Ошибка',
            description: 'В процессе добавления контакта произошла непредвиденная ошибка',
          })
        })
    }
  }

  useEffect(() => {
    if (visible) form.resetFields()
  }, [visible, form])

  return (
    <Modal visible={visible} onOk={() => form.submit()} onCancel={closeModal} cancelText='Отмена'>
      <Typography.Title level={2}>
        {contact ? `${contact.name} ${contact.lastName}` : 'Новый контакт'}
      </Typography.Title>
      <Form
        layout='vertical'
        initialValues={contact || {}}
        form={form}
        onFinish={onFinish}
        autoComplete='off'
      >
        <Form.Item
          label='Имя'
          name='name'
          rules={[{ required: true, message: REQUIRED_MESSAGE }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Фамилия'
          name='lastName'
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Телефон'
          name='phone'
          rules={[{ required: true, message: REQUIRED_MESSAGE }]}
        >
          <Input type='tel' />
        </Form.Item>
        <Form.Item
          label='Адрес'
          name='address'
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Комментарий'
          name='comment'
        >
          <Input.TextArea
            style={{ height: 120 }}
          />
        </Form.Item>
      </Form>
    </Modal>
  )
}
