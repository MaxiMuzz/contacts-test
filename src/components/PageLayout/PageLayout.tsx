import { LoadingOutlined } from '@ant-design/icons'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import styles from './PageLayout.module.scss'

export const PageLayout: React.FC = ({ children }) => {
  const router = useRouter()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const userToken = localStorage.getItem('userToken')
    const path = router.asPath

    if (userToken) {
      if (path === '/sign-in') router.push('/')
      else setLoading(false)
    } else {
      if (path !== '/sign-in') router.push('/sign-in')
      else setLoading(false)
    }
  }, [router, setLoading])

  return (
    <div className={styles.container}>
      {loading ? (
        <div className={styles.loaderWrapper}>
          <LoadingOutlined className={styles.loader} spin />
        </div>
      ) :
        children
      }
    </div>
  )
}
