import type { NextPage } from 'next'
import { ContactsTable } from '../src/components/ContactsTable'
import styles from '../styles/all.module.scss'
import { useEffect, useState } from 'react'
import { notification, Typography } from 'antd'
import api from '../src/api'
import { LoadingOutlined } from '@ant-design/icons'
import { useDispatch, useSelector } from 'react-redux'
import { ContactsAction, ContactsActionTypes } from '../src/store/contactsReducer'
import { Dispatch } from 'redux'
import { IStore } from '../src/store/rootReducer'
import { PageLayout } from '../src/components/PageLayout'

const ContactsPage: NextPage = () => {
  const [isLoading, setIsLoading] = useState(true)
  const contacts = useSelector((state: IStore) => state.contacts)
  const dispatch = useDispatch<Dispatch<ContactsAction>>()

  useEffect(() => {
    api.getContacts().then(({ data, error }) => {
      data && dispatch({ type: ContactsActionTypes.SET, payload: { data } })

      error && notification.error({ 
        message: 'Ошибка',
        description: 'В процессе загрузки данных произошла непредвиденная ошибка',
      })
      setIsLoading(false)
    })
  }, [dispatch])

  return (
    <PageLayout>
      <main className={styles.main}>
        <Typography.Title level={1}>Контакты</Typography.Title>
        {isLoading ? (
            <div className={styles.loading}>
              <LoadingOutlined />
            </div>
          ) : (
            <ContactsTable contacts={contacts} />
          )
        }
      </main>
    </PageLayout>
  )
}

export default ContactsPage
