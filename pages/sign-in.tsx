import { Button, Form, Input, notification, Typography } from 'antd'
import { useRouter } from 'next/router'
import api from '../src/api'
import { UserData } from '../src/api/api'
import { PageLayout } from '../src/components/PageLayout'
import { REQUIRED_MESSAGE } from '../src/constants'
import styles from '../styles/sign-in.module.scss'

const { Title } = Typography

const SignInPage: React.FC = () => {
  const router = useRouter()
  const login = (values: UserData) => {
    api.authorize(values)
      .then(({ data, error }) => {
        if (data) {
          localStorage.setItem('userToken', data.userToken)
          router.push('/')
        }

        error && notification.error({ 
          message: 'Ошибка',
          description: 'Неверный логин или пароль',
        })
      })
  }

  return (
    <PageLayout>
      <main className={styles.card}>
        <Title>Вход</Title>
        <Form
          className={styles.form}
          layout='vertical'
          autoComplete='off'
          onFinish={login}
        >
          <Form.Item
            label='Логин'
            name='login'
            rules={[{ required: true, message: REQUIRED_MESSAGE }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label='Пароль'
            name='password'
            rules={[{ required: true, message: REQUIRED_MESSAGE }]}
          >
            <Input.Password />
          </Form.Item>
          <Button
            className={styles.submit}
            type='primary'
            htmlType='submit'
          >
            Войти
          </Button>
        </Form>
      </main>
    </PageLayout>
  )
}

export default SignInPage
