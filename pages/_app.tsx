import '../styles/globals.scss'
import type { AppProps } from 'next/app'
import 'antd/dist/antd.css'
import { Provider } from 'react-redux'
import store from '../src/store'
import Head from 'next/head'

function ContactsApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Head>
        <title>Ваш список контактов</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Component {...pageProps} />
    </Provider>
  )
}

export default ContactsApp
